package com.backend.metrics.controller;

import com.backend.metrics.dto.RequestMetricsAverageDTO;
import com.backend.metrics.service.AssetService;
import com.backend.metrics.rest.RestClient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class AssetControllerTest {

    private AssetController assetController;
    private AssetService assetService;

    @BeforeEach
    void setUp() {
        assetService = new AssetService(new RestClient());
        assetController = new AssetController(assetService);
    }

    @Test
    public void givenInvalidPeriodShouldReturnNotFound() {
        RequestMetricsAverageDTO requestMetricsAverageDTO = new RequestMetricsAverageDTO();
        requestMetricsAverageDTO.setPeriod(0);
        assertThat(assetController.getAverageMetricsByPeriod(requestMetricsAverageDTO).getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

}
