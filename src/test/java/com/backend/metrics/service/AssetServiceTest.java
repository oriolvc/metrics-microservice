package com.backend.metrics.service;

import com.backend.metrics.dto.AssetMetricsDTO;
import com.backend.metrics.rest.RestClient;
import com.backend.metrics.utils.DateUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

import static org.assertj.core.api.Assertions.*;

@SpringBootTest
public class AssetServiceTest {

    private AssetService assetService;

    @BeforeEach
    void setUp() {
        assetService = new AssetService(new RestClient());
    }

    @Test
    public void testGetMetricsAverage(){
        AssetMetricsDTO givenMetrics = new AssetMetricsDTO();
        givenMetrics.setMetric_3000(new ArrayList<>(Arrays.asList(15.00, 30.00, 45.00)));
        givenMetrics.setMetric_3001(new ArrayList<>(Arrays.asList(10.00, 15.00, 20.00)));
        givenMetrics.setMetric_3002(new ArrayList<>(Arrays.asList(100.00, 100.00, 100.00)));
        givenMetrics.setMetric_3003(new ArrayList<>(Arrays.asList(0.00, 0.00, 0.00)));
        givenMetrics.setMetric_3004(new ArrayList<>(Arrays.asList(33.00, 33.00, 33.00)));
        givenMetrics.setMetric_3005(new ArrayList<>(Arrays.asList(3.00, 3.00, 4.50)));
        LocalDateTime now = LocalDateTime.now();
        givenMetrics.setTime(new ArrayList<>(Arrays.asList(DateUtils.convertLocalDateTimeToDate(now),
                DateUtils.convertLocalDateTimeToDate(now.plusMinutes(1)),
                DateUtils.convertLocalDateTimeToDate(now.plusMinutes(2)))));

        AssetMetricsDTO expectedAverageMetrics = new AssetMetricsDTO();
        expectedAverageMetrics.setMetric_3000(new ArrayList<>(Arrays.asList(30.00)));
        expectedAverageMetrics.setMetric_3001(new ArrayList<>(Arrays.asList(15.00)));
        expectedAverageMetrics.setMetric_3002(new ArrayList<>(Arrays.asList(100.00)));
        expectedAverageMetrics.setMetric_3003(new ArrayList<>(Arrays.asList(0.00)));
        expectedAverageMetrics.setMetric_3004(new ArrayList<>(Arrays.asList(33.00)));
        expectedAverageMetrics.setMetric_3005(new ArrayList<>(Arrays.asList(3.50)));
        expectedAverageMetrics.setTime(new ArrayList<>(Arrays.asList(DateUtils.convertLocalDateTimeToDate(now))));

        Optional<AssetMetricsDTO> result = assetService.getAverageMetrics(givenMetrics, 3);
        assertThat(result.get()).isEqualTo(expectedAverageMetrics);
    }

}
