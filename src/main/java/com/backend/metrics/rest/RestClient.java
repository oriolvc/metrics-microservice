package com.backend.metrics.rest;

import com.backend.metrics.rest.metrics.AssetsWS;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class RestClient {

    private static final String BASE_URL = "https://reference.intellisense.io/";

    private AssetsWS assetsWS;

    public RestClient() {
        Retrofit retrofit = createDefaultClient();
        assetsWS = retrofit.create(AssetsWS.class);
    }


    private Retrofit createDefaultClient(){
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(DeserializationFeature.UNWRAP_ROOT_VALUE);
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(JacksonConverterFactory.create(objectMapper)).build();
    }

    public AssetsWS getTimeseriesService() {
        return assetsWS;
    }
}
