package com.backend.metrics.rest.metrics;

import com.backend.metrics.dto.AssetMetricsDTO;
import retrofit2.Call;
import retrofit2.http.GET;

public interface AssetsWS {

    @GET("/test.dataprovider/")
    Call<AssetMetricsDTO> getAssetMetrics();
}
