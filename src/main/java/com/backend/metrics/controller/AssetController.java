package com.backend.metrics.controller;

import com.backend.metrics.dto.AssetMetricsDTO;
import com.backend.metrics.dto.RequestMetricsAverageDTO;
import com.backend.metrics.service.AssetService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/assets")
public class AssetController {

    private AssetService assetService;

    public AssetController(AssetService assetService) {
        this.assetService = assetService;
    }

    @PostMapping("/metrics/average")
    public ResponseEntity<AssetMetricsDTO> getAverageMetricsByPeriod (@RequestBody RequestMetricsAverageDTO requestMetricsAverageDTO){
        if (requestMetricsAverageDTO.getPeriod() < 1){
            return ResponseEntity.badRequest().build();
        }
        Optional<AssetMetricsDTO> assetMetricsDTO = assetService.getMetrics();
        if (!assetMetricsDTO.isPresent()){
            return ResponseEntity.notFound().build();
        }
        Optional<AssetMetricsDTO> assetMetricsAverageDTO = assetService.getAverageMetrics(assetMetricsDTO.get(), requestMetricsAverageDTO.getPeriod());
        return assetMetricsAverageDTO.isPresent()
                ? ResponseEntity.ok(assetMetricsAverageDTO.get())
                : ResponseEntity.internalServerError().build();
    }
}
