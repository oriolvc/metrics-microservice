package com.backend.metrics.service;

import com.backend.metrics.dto.AssetMetricsDTO;
import com.backend.metrics.rest.RestClient;
import org.apache.commons.collections4.ListUtils;
import org.springframework.stereotype.Service;
import retrofit2.Call;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;

@Service
public class AssetService {

    private RestClient restClient;

    public AssetService(RestClient restClient) {
        this.restClient = restClient;
    }

    public Optional<AssetMetricsDTO> getMetrics(){
        Call<AssetMetricsDTO> call = this.restClient.getTimeseriesService().getAssetMetrics();
        AssetMetricsDTO assetMetrics = null;
        try {
            assetMetrics = call.execute().body();
        } catch (IOException e) {
            return Optional.empty();
        }
        return Optional.of(assetMetrics);
    }

    public Optional<AssetMetricsDTO> getAverageMetrics(AssetMetricsDTO metricsDTO, Integer period) {
        AssetMetricsDTO result = new AssetMetricsDTO();
        result.setMetric_3000(calculateMetricAverage(metricsDTO.getMetric_3000(), period));
        result.setMetric_3001(calculateMetricAverage(metricsDTO.getMetric_3001(), period));
        result.setMetric_3002(calculateMetricAverage(metricsDTO.getMetric_3002(), period));
        result.setMetric_3003(calculateMetricAverage(metricsDTO.getMetric_3003(), period));
        result.setMetric_3004(calculateMetricAverage(metricsDTO.getMetric_3004(), period));
        result.setMetric_3005(calculateMetricAverage(metricsDTO.getMetric_3005(), period));
        result.setTime(recalculateTime(metricsDTO.getTime(), period));

        return Optional.of(result);
    }

    private List<Double> calculateMetricAverage(List<Double> metrics, Integer period){
        List<Double> result = new ArrayList<>();
        List<List<Double>> valuesGroupedByPeriod = ListUtils.partition(metrics, period);
        for (List<Double> values : valuesGroupedByPeriod){
            result.add(values.stream()
                    .filter(Objects::nonNull)
                    .mapToDouble(x -> x)
                    .average()
                    .orElse(Double.NaN));
        }
        return result;
    }

    private List<Date> recalculateTime(List<Date> metrics, Integer period){
        List<Date> result = new ArrayList<>();
        int index = 0;
        while (index < metrics.size()){
            result.add(metrics.get(index));
            index = index + period;
        }
        return result;
    }


}
