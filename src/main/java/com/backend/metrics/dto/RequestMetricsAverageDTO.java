package com.backend.metrics.dto;

public class RequestMetricsAverageDTO {

    private Integer period;

    public Integer getPeriod() {
        return period;
    }

    public void setPeriod(Integer period) {
        this.period = period;
    }
}
