package com.backend.metrics.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@JsonRootName(value = "660")
public class AssetMetricsDTO {

    @JsonProperty("3000")
    private List<Double> metric_3000;

    @JsonProperty("3001")
    private List<Double> metric_3001;

    @JsonProperty("3002")
    private List<Double> metric_3002;

    @JsonProperty("3003")
    private List<Double> metric_3003;

    @JsonProperty("3004")
    private List<Double> metric_3004;

    @JsonProperty("3005")
    private List<Double> metric_3005;

    private List<Date> time;



    public List<Date> getTime() {
        return time;
    }

    public void setTime(List<Date> time) {
        this.time = time;
    }

    public List<Double> getMetric_3000() {
        return metric_3000;
    }

    public void setMetric_3000(List<Double> metric_3000) {
        this.metric_3000 = metric_3000;
    }

    public List<Double> getMetric_3001() {
        return metric_3001;
    }

    public void setMetric_3001(List<Double> metric_3001) {
        this.metric_3001 = metric_3001;
    }

    public List<Double> getMetric_3002() {
        return metric_3002;
    }

    public void setMetric_3002(List<Double> metric_3002) {
        this.metric_3002 = metric_3002;
    }

    public List<Double> getMetric_3003() {
        return metric_3003;
    }

    public void setMetric_3003(List<Double> metric_3003) {
        this.metric_3003 = metric_3003;
    }

    public List<Double> getMetric_3004() {
        return metric_3004;
    }

    public void setMetric_3004(List<Double> metric_3004) {
        this.metric_3004 = metric_3004;
    }

    public List<Double> getMetric_3005() {
        return metric_3005;
    }

    public void setMetric_3005(List<Double> metric_3005) {
        this.metric_3005 = metric_3005;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AssetMetricsDTO that = (AssetMetricsDTO) o;
        return metric_3000.equals(that.metric_3000) && metric_3001.equals(that.metric_3001) && metric_3002.equals(that.metric_3002) && metric_3003.equals(that.metric_3003) && metric_3004.equals(that.metric_3004) && metric_3005.equals(that.metric_3005) && time.equals(that.time);
    }

    @Override
    public int hashCode() {
        return Objects.hash(time);
    }
}
