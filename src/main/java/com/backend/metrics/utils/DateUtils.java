package com.backend.metrics.utils;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class DateUtils {

    public static Timestamp convertLocalDateTimeToDate(LocalDateTime localDateTime) {
        return Timestamp.valueOf(localDateTime);
    }
}
