Instructions:

1. Having docker configured, open CLI and execute step 2 and 3
2. docker pull oriolvc/metrics-microservice
3. docker run -p 8080:8080 oriolvc/metrics-microservice
4. navigate to http://localhost:8080/swagger-ui/
